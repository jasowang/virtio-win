#!/bin/sh

drivers=(viostor vioserial vioscsi viorng vioinput pvpanic NetKVM Balloon viofs)
oses=(2k12 2k12R2 2k16 2k19 w10 w8.1 w8)
platfroms=(amd64 x86)
COMPARE_FILE=installed_signed_versions.txt
RESULT_FILE=compare_result.txt

echo "Compare versions in INF files"

rm $COMPARE_FILE
cd mnt_iso
for driver in ${drivers[@]}; do
    cd $driver
    for os in ${oses[@]}; do
        for p in ${platfroms[@]}; do
            if [ $os=="w8" ]; then
                grep -Rr --include="*.inf" DriverVer | grep "$os" | grep "$p" | grep -v "w8.1" >> ../../$COMPARE_FILE
            else
                grep -Rr --include="*.inf" DriverVer | grep "$os" | grep "$p" >> ../../$COMPARE_FILE
            fi
        done
    done
    cd ..
done
cd ..

diff signed_drivers_versions.txt $COMPARE_FILE > $RESULT_FILE

if [ -s $RESULT_FILE ]
then
    echo "Failure: versions of some of the drivers are different!"
    cat $RESULT_FILE
    exit 1
else
    echo "Success: INF versions are correct."
    exit 0
fi
